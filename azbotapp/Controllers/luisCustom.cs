﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;

namespace azbotapp.Controllers
{
    [LuisModel("d749737a-193b-4aa7-af64-fdcd83d91308", "4247f4e0f77444859f3a6d0632cbfef0")]
    [Serializable]
    public class luisCustom : LuisDialog<object>
    {
        [LuisIntent("")]
        public async Task None(IDialogContext context, LuisResult result)
        {
            string message = "很抱歉!我無法理解您的意思!";
            await context.PostAsync(message);
            //context.Wait(MessageReceived);
            context.Done<object>(new object());
        }

        [LuisIntent("天氣預報")]
        public async Task GetForcast(IDialogContext context, LuisResult result)
        {
            string message = "您好!明日下雨機率90%";
            await context.PostAsync(message);
            //context.Wait(MessageReceived);
            context.Done<object>(new object());
        }

        [LuisIntent("天氣現況")]
        public async Task GetCondition(IDialogContext context, LuisResult result)
        {
            string message = "您好!目前天氣不錯";
            await context.PostAsync(message);
            //context.Wait(MessageReceived);
            context.Done<object>(new object());
        }


        //public async Task StartAsync(IDialogContext context)
        //{
        //    context.Wait(MessageReceived);
        //}




    }
}